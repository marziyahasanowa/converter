package com.example.mylibrary.models.request

data class DataRequest(
    val date: String,
    val apiKey: String,
    val base: String
)
