package com.example.converter.data.models

data class Currency(
    val name: String,
    val rate: Double,
    val isUpdated: Boolean?
)
