package com.example.converter.data

import com.example.converter.data.models.Data
import com.example.mylibrary.models.request.DataRequest

interface ConverterRepository {
    suspend fun getCurrencyOnDate(
        params: DataRequest
    ): Data
}