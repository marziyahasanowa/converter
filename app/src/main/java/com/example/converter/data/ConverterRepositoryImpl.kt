package com.example.converter.data

import com.example.converter.data.models.Data
import com.example.converter.network.CurrencyApiService
import com.example.mylibrary.models.request.DataRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ConverterRepositoryImpl (
    private val currencyApiService: CurrencyApiService
): ConverterRepository {
    override suspend fun getCurrencyOnDate(params: DataRequest): Data =
        withContext(Dispatchers.IO){
            with(params) {
                currencyApiService.getCurrencyOnDate(date, apiKey, base)
            }
        }
}