package com.example.converter

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources.Theme
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat.ThemeCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.converter.data.models.Currency
import com.example.converter.databinding.CurrencyItemBinding

class CurrencyAdapter(
    private var currencies: List<Currency>,
) : RecyclerView.Adapter<CurrencyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = CurrencyItemBinding.inflate(inflater, parent, false)
        return CurrencyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(currencies[position])
    }

    override fun getItemCount(): Int = currencies.size

    @SuppressLint("NotifyDataSetChanged")
    fun updateCurrencies(newCurrencies: List<Currency>) {
        currencies = newCurrencies
        notifyDataSetChanged()
    }

}

class CurrencyViewHolder(
    private val binding: CurrencyItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(currency: Currency) {
        binding.name.text = currency.name
        binding.rate.text = currency.rate.toString()
        if (currency.isUpdated == true) {
            itemView.setBackgroundColor(itemView.resources.getColor(R.color.green, binding.root.context.theme))
        } else {
            itemView.setBackgroundColor(itemView.resources.getColor(R.color.red, binding.root.context.theme))
        }
    }
}