package com.example.converter

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.converter.data.models.Currency
import com.example.converter.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val currencies = mutableListOf<Currency>()
    private val recyclerView: RecyclerView by lazy { binding.recyclerView }
    private val adapter = CurrencyAdapter(currencies)
    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        viewModel.getCurrencies(getString(R.string.default_base))
        viewModel.currencies.observe(this) { currencies ->
            val list =  currencies?.map { it }?.toList()
            if (list != null) {
                adapter.updateCurrencies(list)
            }
        }

        val spinner: Spinner = binding.spinner

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val baseCurrency = spinner.selectedItem.toString()
                viewModel.getCurrencies(baseCurrency)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                val baseCurrency = getString(R.string.default_base)
                viewModel.getCurrencies(baseCurrency)
            }
        }

    }
}