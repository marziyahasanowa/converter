package com.example.converter.domain

import com.example.mylibrary.models.request.DataRequest

class CurrenciesInteractor(
    private val useCase: GetCurrenciesUseCase
) {

    suspend fun getCurrencies(params: DataRequest) = useCase.getCurrencies(params)

}