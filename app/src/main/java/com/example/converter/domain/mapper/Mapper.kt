package com.example.converter.domain.mapper

interface Mapper <P, R> {

    fun map(params: P): R
}