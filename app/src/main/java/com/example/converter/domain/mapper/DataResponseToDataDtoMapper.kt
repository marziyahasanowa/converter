package com.example.converter.domain.mapper

import com.example.converter.data.models.Data
import com.example.converter.domain.model.DataDto

class DataResponseToDataDtoMapper : Mapper <Data, DataDto>{
    override fun map(params: Data): DataDto =
        with(params){
            DataDto(base, date, historical, rates, success, timestamp)
        }
}