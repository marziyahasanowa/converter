package com.example.converter.domain

import com.example.converter.data.ConverterRepository
import com.example.converter.data.models.Data
import com.example.converter.domain.mapper.DataResponseToDataDtoMapper
import com.example.converter.domain.model.DataDto
import com.example.mylibrary.models.request.DataRequest

class GetCurrenciesUseCase(
    private val repository: ConverterRepository,
    private val mapper: DataResponseToDataDtoMapper
) {
    suspend fun getCurrencies(params: DataRequest): DataDto =
        mapper.map(repository.getCurrencyOnDate(params))
}