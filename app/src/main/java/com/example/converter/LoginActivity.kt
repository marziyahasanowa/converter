package com.example.converter

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.converter.databinding.ActivityLoginBinding
import java.util.Calendar
import kotlin.random.Random

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val username = binding.editTextUsername.text.toString()
        val password = binding.editTextPassword.text.toString()

        val accessToken = generateAccessToken()

        val expirationTime = Calendar.getInstance().apply {
            add(Calendar.MINUTE, 10)
        }.timeInMillis

        binding.buttonLogin.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}

fun generateAccessToken() {
    val chars = "qwertyuiopasdfghjklzxcvbnm1234567890"
    val tokenLength = 20

    repeat(tokenLength) {
        val randomIndex = Random.nextInt(chars.length)
        StringBuilder().append(chars[randomIndex])
    }
}