package com.example.converter

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.converter.data.ConverterRepositoryImpl
import com.example.converter.data.models.Currency
import com.example.converter.domain.CurrenciesInteractor
import com.example.converter.domain.GetCurrenciesUseCase
import com.example.converter.domain.mapper.DataResponseToDataDtoMapper
import com.example.converter.network.RetrofitClient
import com.example.mylibrary.models.request.DataRequest
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import java.util.TimeZone

private const val API_KEY = "LckMpBzpJF6mreYXSCPQED1tmKXlFr5o"
private const val DATE_PATTERN = "yyyy-MM-dd"
private const val TIME_ZONE_ID = "GMT"
private const val AMOUNT_OF_DAYS = -1

class MainViewModel : ViewModel() {

    private val apiService = RetrofitClient.apiService

    private var todayCurrencies = MutableLiveData<List<Currency>>()
    val currencies: LiveData<List<Currency>> = todayCurrencies

    private var _isUpdated = MutableLiveData<Boolean>()

    private val currenciesInteractor = CurrenciesInteractor(
        GetCurrenciesUseCase(
            ConverterRepositoryImpl(RetrofitClient.apiService),
            DataResponseToDataDtoMapper()
        )
    )

    fun getCurrencies(base: String){
        viewModelScope.launch {
            try {
                val todayDataRequest = currenciesInteractor.getCurrencies(DataRequest(getCurrentDate(), API_KEY, base))
                val yesterdayDataRequest = currenciesInteractor.getCurrencies(DataRequest(getYesterdayDate(), API_KEY, base))

                val currencies = mutableListOf<Currency>()

                for (key in todayDataRequest.rates.keys) {
                    if (yesterdayDataRequest.rates.containsKey(key)) {
                        val todayRate = todayDataRequest.rates[key] ?: error("today rate not found for key $key")
                        val yesterdayRate = yesterdayDataRequest.rates[key] ?: error("yesterday rate not found for key $key")

                        val isUpdated = todayRate != yesterdayRate
                        _isUpdated.postValue(isUpdated)

                        currencies.add(Currency(key, todayRate, isUpdated))
                    }
                }
                todayCurrencies.postValue(currencies)
            } catch (e: Exception) {
                Log.e("responseTag", "Error: ${e.message}")
            }
        }
    }

    private fun getCurrentDate(): String {
        val dateFormat = SimpleDateFormat(DATE_PATTERN, Locale.getDefault())
        dateFormat.timeZone = TimeZone.getTimeZone(TIME_ZONE_ID)
        return dateFormat.format(Date())
    }

    private fun getYesterdayDate(): String {
        val calendar = Calendar.getInstance(TimeZone.getTimeZone(TIME_ZONE_ID))
        calendar.add(Calendar.DATE, AMOUNT_OF_DAYS)
        val dateFormat = SimpleDateFormat(DATE_PATTERN, Locale.getDefault())
        dateFormat.timeZone = TimeZone.getTimeZone(TIME_ZONE_ID)
        return dateFormat.format(calendar.time)
    }
}
